import { useEffect, useState } from 'react';
import { Command } from './Command';
import response from '../../database/git-command-list.json';
import logo from '../../assets/img/logo/git.png';

function CommandSearcher({ commandSearched, onSearch }) {
  return (
    <div className='container'>
      <div className='d-flex justify-content-end py-4 px-2'>
        <div className='form bg-darsk p-4 px-0 '>
          <input
            type='search'
            className='form-control rounded d-inline-block border-primary'
            onChange={(evento) => onSearch(evento.target.value)}
            placeholder='Comando a consultar... '
            style={{ width: '360px' }}
            value={commandSearched}
            autoFocus
          />
        </div>
      </div>
    </div>
  );
}

export function Comandos() {
  const [commands, setCommands] = useState([]);
  const [commandTyped, onType] = useState('');
  let commandView = commands;

  useEffect(function () {
    setCommands(response.commands);
    console.log(response);
  }, []);

  if (commandTyped && commands) {
    commandView = commands.filter((command) => {
      let nameLowerCase = command.name.toLowerCase();
      let typedLowerCase = commandTyped.toLocaleLowerCase();
      return nameLowerCase.includes(typedLowerCase);
    });
  }

  return (
    <div>
      <h1 className='text-center py-1' style={{ color: '#004B8F' }}>
        COMANDOS DE
        <img
          src={logo}
          alt='image'
          style={{ height: '140px', width: '140px', objectFit: 'cover' }}
        />
      </h1>
      <CommandSearcher commandSearched={commandTyped} onSearch={onType} />

      <div className='row'>
        {commandView
          ? commandView.map(function (command) {
              return <Command key={command.id} {...command} />;
            })
          : 'Cargando elementos...'}
      </div>
    </div>
  );
}
